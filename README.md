# ouf2mp

ufo2mp est un petit script permettant de générer un fichier metapost (mp) à partir d'un fichier ufo.  Le fichier glyphs.mp est à utiliser dans le kit de survie metapost (https://gitlab.com/erg-type/metapost.survival-kit), permettant ainsi de metaposter à partir d'une typo existante (en gardant les courbes de bézier)

![ufo.svg](ufo.svg)

## Dépendances
* Python (ici Python3)
* lxml : `pip3 install lxml`

## Structure

```
├── ouf2mp.py // le fameux script
│  
├── ufo
│   ├── A_.glif
│   ├── B_.glif
│   ├── C_.glif
│   ├── a_.glif
│   ├── b_.glif
│   └── c_.glif
│
└── mp / où le fichier mp est généré
    └── glyphs.mp // fichier généré

```
## Utilisation
- Récupérer fichiers .glifs (Afficher contenu du paquet du fichier .ufo)
- Les mettre dans dossier ufo/
- Lancer le script : `python3 ouf2mp.py`

## Arguments
- Type de la typo (stroke ou outline)
  * Pour une stroke font : `python3 ouf2mp.py stroke`
  * Pour une outline font : `python3 ouf2mp.py outline`

- Type de fichier à générer
  * Un fichier .mp avec tous les glyphes : `python3 ouf2mp.py single`
  * Un fichier .mp = un glyphe : `python3 ouf2mp.py multiple`


## Variables modifiable dans le script
- `coeff` = divise les coordonnées x et y (par défault 1)
- `file` = le fichier à générer (par défault mp/glyphs.mp)
- `pen` = forme de la plume (variable qui peut être définie dans le fichier global)

## Binz
Problèmes avec l'encodage de certains glyphes (alternates / symboles spéciaux)
