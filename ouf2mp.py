import sys
import lxml.etree as ET
import array as arr
import re
import os
import glob

#arguments par défault
output = 'single'
cycle = ''

#on recup les Arguments
#si contient 0 ou 1
if 'single' in sys.argv:
    output = 'single'
elif 'multiple' in sys.argv:
    output = 'multiple'
#si contient stroke ou outline
if 'stroke' in sys.argv:
    cycle = ''
elif 'outline' in sys.argv:
    cycle = '..cycle'

#fichier où on souhaite écrire les coordonnées metapost
file = 'mp/glyphs.mp'
#eventuel coefficient pour diviser/mulitplier les coordonnées obtenues
coeff = 10
#definir pen
pen = 'penCanne;'

#fonction qui va écrire les coordonnées metapost dans le fichier file
def buildMp(lettre):

        #parsing des .glif
        tree = ET.parse(lettre)
        root = tree.getroot()
        outline = root.find('outline')

        if root.find('unicode') != None:
            print(root.find('unicode'))



            hex = root.find('unicode').get('hex')

            #code hex
            hex = 'U+' + hex

            #convertir code hex en char
            char = chr(int(hex[2:], 16))

            #i = incrémentente à chaque point
            i = 0

            #i = permet de déterminer si courbe bézier ou point
            bezier = '1'

            #get la width du glyph (ici avec arrondi)
            width = round(int(root.find('advance').get('width')) / coeff, 1)

            #beginchar déclare le début du dessin du glyphe
            begin_char = 'beginchar('+ str(ord(char))+',' + str(width) +')'

            #current_glyph stock coordonnées x y
            current_glyph = ''

            #draw_z stocke dessin entre les coordonnées
            draw_z = ''

            #on choppe tous les contour (nouveau contour = nouveau draw dès qu'on "relève" le crayon)
            if outline:
                for contour in outline.findall('contour'):
                    draw_z += '\t draw '
                    first = True


                    for point in contour.findall('point'):

                        x = round(float(point.get('x')) / coeff, 1)
                        y = round(float(point.get('y')) / coeff, 1)

                        type = point.get('type')

                        if (type == 'curve' or  type == 'move' or type == 'line'):

                            i = i + 1;
                            bezier = '1';

                            current_glyph += '\t x'+ str(i) + ' := ' + str(x) +' * ux;\n'
                            current_glyph += '\t y'+ str(i) + ' := ' + str(y) +' * uy;\n'

                            if first == True:
                                draw_z += 'z'+ str(i)
                                first = False
                            else:
                                if (type == 'curve' or type == 'move'):
                                    draw_z += '..z'+ str(i)
                                elif type == 'line':
                                    draw_z += '--z'+ str(i)

                        else:
                            current_glyph += '\t x'+ str(i) +'_'+bezier+ ' := ' + str(x) +' * ux;\n'
                            current_glyph += '\t y'+ str(i) +'_'+bezier+ ' := ' + str(y) +' * uy;\n'
                            if bezier == '1':
                                draw_z += '..controls z'+ str(i) +'_'+bezier
                            else:
                                draw_z += ' and z'+ str(i) +'_'+ bezier

                            bezier = '2';


                    draw_array = draw_z.split('draw')
                    #for draw in draw_array:
                        #print(draw)
                        #testDraw = draw.replace('..', ' ').replace('--', ' ').split(' ')
                        #print(testDraw)
                        #if testDraw[-2] == 'and':
                            #print('hehe')
                            #print(testDraw[1])
                            #draw_z += '..' + testDraw[1]

                        #current_draw == draw.split(' ')
                        #print(current_draw[-2])

                            #print('lol')

                        #draw_firstZ = draw_array[2].split('.')
                        #print(draw_array)
                        #draw_z += '..done'


                    draw_z += cycle +' withcolor col; \n'
                    print(draw_z)






            meta_glyph = '%' + char + '\n' + begin_char + '\n' + current_glyph + '\n' + pen + '\n' + draw_z + '\n endchar(' + str(i) +');\n\n'
            if output == 'single':
                f = open(file, "a")
                f.write(meta_glyph)
                f.close()
            elif output == 'multiple':
                f = open('mp/' + str(ord(char)) + '.mp', "w")
                f.write(meta_glyph)
                f.close()



# # # # # # # # # #

#on clean le fichier glif
f = open(file, "w")
f.write('')

for filename in glob.glob('ufo/*.glif'):
    with open(os.path.join(os.getcwd(), filename), 'r') as lettre:

        buildMp(lettre)

#on oublie pas de déclarer end
f = open(file, "a")
f.write('\n end;')
f.close()
